<?php
/*
Template Name: Page à propos
Template Post Type: page
*/
?>

<?php get_header(); ?>

<?php 

    $image_profil = get_field('about-img');
    $about_column1 = get_field('about-column1');
    $about_column2 = get_field('about-column2');
    $about_column3 = get_field('about-column3');

    $info_column1 = get_field('info-column1');
    $info_column2 = get_field('info-column2');
    $info_column3 = get_field('info-column3');

?>

<main>
    <section class="header-about">

            <div class="header-photo">
                <img src="<?php echo $image_profil['url'] ?>" alt="<?php echo $image_profil['alt'] ?>">
            </div>

            <div class="header-title">
                <h2><?php the_field('about-title') ?></h2>
            </div>

            <div class="header-content">
                <div class="content-item">
                    <h3><?php echo $about_column1['column-title'] ?></h3>
                    <h4><?php echo $about_column1['column-subtitle'] ?></h4>
                    <p><?php echo $about_column1['column-txt'] ?></p>
                </div>
                <div class="content-item">
                    <h3><?php echo $about_column2['column-title'] ?></h3>
                    <h4><?php echo $about_column2['column-subtitle'] ?></h4>
                    <p><?php echo $about_column2['column-txt'] ?></p>
                </div>
                <div class="content-item">
                    <h3><?php echo $about_column3['column-title'] ?></h3>
                    <h4><?php echo $about_column3['column-subtitle'] ?></h4>
                    <p><?php echo $about_column3['column-txt'] ?>.</p>
                </div>
            </div>
            
    </section>

    <section class="body-about">

        <div class="body-header">
            <h2><?php the_field('title-section') ?></h2>
        </div>

        <div class="body-content part1">
            <div class="content">
                <h3><?php echo $info_column1['column-title'] ?></h3>
                <p><?php echo $info_column1['column-txt'] ?></p>
            </div>
            <div class="img">
                <img src="<?php echo $info_column1['column-img']; ?>" alt="tristan-tornatore-digital-freelance-wordpress-seo-graphisme">
            </div>
        </div>

        <div class="body-content part2">
        <div class="content">
                <h3><?php echo $info_column2['column-title'] ?></h3>
                <p><?php echo $info_column2['column-txt'] ?></p>
            </div>
            <div class="img">
                <img src="<?php echo $info_column2['column-img']; ?>" alt="tristan-tornatore-digital-freelance-wordpress-seo-graphisme">
            </div>
        </div>

        <div class="body-content part3">
                <div class="content">
                <h3><?php echo $info_column3['column-title'] ?></h3>
                <p><?php echo $info_column3['column-txt'] ?></p>
                </div>
                <div class="img">
                    <img src="<?php echo $info_column3['column-img']; ?>" alt="tristan-tornatore-digital-freelance-wordpress-seo-graphisme">
                </div>
        </div>

    </section>

    <section class="home_cta master_cta">
        <div class="text-cta">
            <p><?php the_field('ctamaster-txt') ?></p>
        </div>
        <a href="<?php the_field('ctamaster-link') ?>" class="btn-master"><p>Proposer un projet</p></a>
    </section>


</main>

<?php get_footer(); 
