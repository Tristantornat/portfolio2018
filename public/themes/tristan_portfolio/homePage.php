<?php
/*
Template Name: Page d'accueil
Template Post Type: page
*/
?>

<?php get_header(); ?>


<main>

    <?php
        //Variables 
        $header = get_field('home-header');
        $sectionFreelance = get_field('section_freelance');
        $columnHome1 = get_field('column1');
        $columnHome2 = get_field('column2');
        $columnHome3 = get_field('column3');
    ?>


    <section class="home_main">
        <div class="container-image">
            <img src="<?php echo $header['img'] ?>" alt="freelance-digital-wordpress-tristan-tornatore">
        </div>
        
        <div class="main-header">
            <h1><?php echo $header['title'] ?></h1>
            <h2><?php echo $header['subtitle'] ?></h2>
            <p><?php echo $header['text'] ?></p>
            <a href="<?php echo $header['link'] ?>" class="btn-primary"><p><?php echo $header['cta-text'] ?></p></a>
        </div>
    </section>

    <section class="home_text">
        <h2><?php echo $sectionFreelance['title'] ?></h2>
        <p class="text-desc"><?php echo $sectionFreelance['text'] ?></p>
        <p><span><?php echo $sectionFreelance['span'] ?></span></p>
        <a href="<?php echo $sectionFreelance['cta-link'] ?>" class="btn-primary"><p><?php echo $sectionFreelance['cta-text'] ?></p></a>
    </section>

    <section class="portfolio" id="realisations">

        <div class="portfolio-items">

            <div class="portfolio-header">
                <h2>Mes dernières réalisations</h2>
            </div>

                <?php 

                    $posts = get_posts(array(
                        'posts_per_page'	=> 4,
                        'post_type'			=> 'portfolio'
                    ));

                    if( $posts ): ?>
                        
                            
                        <?php foreach( $posts as $post ): 
                            
                            setup_postdata( $post );
                            get_template_part('portfolio-item');
                            ?>
                        
                        <?php endforeach; ?>
                        
            
                <?php wp_reset_postdata(); ?>

             <?php endif; ?>
        </div>

    </section>

    <section class="home_prestation">
        <div class="home_prestation-header">
            <h2><?php the_field('title-expert') ?></h2>
        </div>
        <div class="expertises-items">
            <div class="home_prestation-body prestation-item">
                <img src="http://localhost:8888/uploads/2018/09/fond-bann_Icone-Stratégie-e1537966023597.png" alt="">
                <h3>Stratégie digitale</h3>
                <h4>SEO - SEA - UX </h4>
                <p>Mettez votre site en valeur avec une stratégie de référencement naturel. Soyez là où sont vos utilisateurs grâce à des campagnes sponsorisée. Et surtout je vous aide à offrir la meilleure expérience utilisateur possible à vos visiteurs. Audit UX.</p>
            </div>
            <div class="home_prestation-body prestation-item">
                <img src="http://localhost:8888/uploads/2018/09/fond-bann_Icone-dev-e1537966009435.png" alt="">
                <h3>Développement web</h3>
                <h4>WebDesign - Wordpress - ReactJs</h4>
                <p>Soyez sûr de choisir la bonne technologie pour votre site web. Ne doutez plus, profitez de mes conseils pour monter ensemble votre cahier des charges. Réalisation de site sous wordpress. Allons p</p>
            </div>
            <div class="home_prestation-body prestation-item">
                <img src="http://localhost:8888/uploads/2018/09/fond-bann_Icone-création-e1537965998679.png" alt="">
                <h3>Graphisme et vidéo</h3>
                <h4>Illustrator - Photoshop - After effects</h4>
                <p>Racontez votre histoire pour vous démarquer. Offrez-vous l'image qui vous correspond. On élabore ensemble une charte graphique porteuse de sens et La proposition de valeur qui fera mouche auprès de votre audience.</p>
            </div>
        </div>
        
    </section>

    <section class="home_cta master_cta">
        <div class="text-cta">
            <p><?php echo the_field('ctamaster') ?></p>
        </div>
        <a href="<?php echo the_field('ctamaster-link') ?>" class="btn-master"><p>Proposez votre projet</p></a>
    </section>

</main>





<?php get_footer();