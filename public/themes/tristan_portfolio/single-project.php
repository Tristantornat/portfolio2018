<?php
/*
Template Name: Page projet
Template Post Type: page
*/
?>

<?php get_header(); ?>

<main>

    <section class="projet-header">
        <h1>Boostez votre projet digital</h1>
        <p>Vous souhaitez être accompagné sur votre projet digital ? Nous pouvons échanger ensemble pour prendre les bonnes décisions. Je vous propose mon professionnalisme, mes compétences et ma motivation pour faire de votre projet une réussite.  </p>
    </section>

    <section class="form">
        <?php echo do_shortcode('[contact-form-7 id="62" title="Form material projet"]'); ?>    
    </section>

    <section class="home_cta master_cta">
        <div class="text-cta">
            <p>Vous souhaitez me contacter directement ?</p>
        </div>
        <a href="http://localhost:8888/contact/" class="btn-master"><p>Contacter Tristan</p></a>
    </section>

</main>

<?php get_footer(); 