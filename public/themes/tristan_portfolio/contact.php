<?php
/*
Template Name: Page Contact
Template Post Type: page
*/
?>

<?php get_header(); ?>

<main>
    <section class="contact">

        <section class="contact-header">
            <h1>Tristan Tornatore, le freelance digital disponible pour vos projets</h1>
            <p class="title-contact">Toujours prêt à répondre à toutes vos questions et discuter autour de vos projets digitaux !</p>
            <p>Vous avez des questions ? Vous souhaitez un conseil gratuit pour mieux réaliser vos projets digitaux ? N'hésitez pas à me contacter. Vous pouvez me joindre directement ou demander à être rappelé. Réponse rapide garantie !</p>
        </section>

        <section class="cart-contact">
                <p><span>email :</span> contact@tornatore.fr</p>
                <p><span>téléphone :</span> 06 31 49 63 05</p>
                <p><span>adresse :</span> 1 Rue de la crèche - 35000 - Rennes</p>
        </section>

    </section>
    
</main>

<?php get_footer(); 