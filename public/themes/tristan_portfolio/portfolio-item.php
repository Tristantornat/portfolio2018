<div class="portfolio-content <?php the_field('portfolio_class') ?>">
    <div class="content-img">
        <?php $portfolio_image = get_field('portfolio_img') ?>
        <img src="<?php echo $portfolio_image['url'] ?>" alt="<?php echo $portfolio_image['alt'] ?>">
        <div class="img-content">
            <p><?php the_field('icone_insight'); the_field('portfolio_insight') ?></p>
        </div>
    </div>
    <div class="content-txt">
        <h2><?php the_field('portfolio_title') ?></h2>
        <p><?php the_field('portfolio_description') ?></p>
        <a href="<?php the_field('portfolio_link') ?>" target="_blank"><?php the_field('portfolio_cta') ?></a>
    </div>
    
</div>