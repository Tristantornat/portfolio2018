<?php 
  $image_profil = get_field('photo_profil', 'option');
?>

<footer>

    <div class="footer-content">
      <div class="info_freelance">
        <div class="detail_freelance">
          <img src="<?php echo $image_profil['url'] ?>" alt="<?php echo $image_profil['alt'] ?>">
        </div>
        
          <div class="description_freelance">
            <h4>À la conquète du web</h4>
            <p>Je suis Tristan Tornatore, freelance dans le domaine du digital. Basé à Rennes, j’interviens à distance et je peux me déplacer partout en France.</p>
            <div class="social-media">
              <div class="twitter">
                <a href="https://www.twitter.com/tristantornat/" target="_blank"><i class="fab fa-twitter"></i></a>
              </div>
              <div class="linkedin">
                <a href="https://www.linkedin.com/in/tristantornat/" target="_blank"><i class="fab fa-linkedin"></i></a>
              </div>
          </div>
          </div>

          
      </div>

      <div class="sitemap col-links">
        <h4>Ailleurs sur ce site</h4>
        <div class="links_col"></div>
        <?php wp_nav_menu( array( 'theme_location' => 'footer-navigation' ) ); ?>
      </div>

      <div class="legal-mention col-links">
        <h4>Mentions légales</h4>
        <div class="links_col"></div>
        <?php wp_nav_menu( array( 'theme_location' => 'footer-mentions' ) ); ?>
      </div>
      
    </div>
    

    <div class="copyright">
      <p>© 2018 - Tristan Tornatore - Site réalisé par mes soins à Rennes</p>
    </div>
  </footer>
  
  <?php wp_footer(); ?>
</body>
</html>
