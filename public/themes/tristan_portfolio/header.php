<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="theme-color" content="#6d9aea">

  <link rel="shortcut icon" href="http://localhost:8888/uploads/2018/10/favicon.png" />


  <!-- Font Awesome --> 
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

  <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

    <?php 
        $image_logo = get_field('logo_header', 'option');
    ?>

    <header class="wrapper cd-morph-dropdown">

        <a href="#0" class="nav-trigger">Open Nav<span aria-hidden="true"></span></a>

        <div class="header-logo">
           <a href="http://localhost:8888/"><img src="<?php echo $image_logo['url'] ?>" alt="<?php echo $image_logo['alt'] ?>"></a> 
        </div>

        <nav>
            <ul>
                <li><a href="http://localhost:8888/#realisations">réalisations</a></li>
                <li><a href="http://localhost:8888/a-propos-du-freelance-tristan/">à propos</a></li>
                
                <!--<li><a href="">prestations</a></li>-->
                <li class="cta-primary"><a href="http://localhost:8888/proposer-un-projet-freelance/">proposer un projet</a></li>
                <li><a href="http://localhost:8888/contact/">contact</a></li>
            </ul>
        </nav>

        <div class="morph-dropdown-wrapper">
            <div class="dropdown-list">
                <h4><a href="http://localhost:8888/">Tristan - Freelance Digital</a></h4>
                <hr/>
                <ul>
                    <li><a href="">réalisations</a></li>
                    <li><a href="http://localhost:8888/a-propos-du-freelance-tristan/">à propos</a></li>
                    <!--<li><a href="">prestations</a></li>-->
                    <li><a href="http://localhost:8888/contact/">contact</a></li>
                    <li class="cta-primary"><a href="http://localhost:8888/proposer-un-projet-freelance/">proposer un projet</a></li>
                </ul>
            </div>
            <div class="social-media">
                <a href="#"><i class="fab fa-twitter"></i></a>
                <a href="#"><i class="fab fa-linkedin"></i></a>
          </div>
            <div class="bg-layer" aria-hidden="true"></div>
            
        </div>

        

    </header>
